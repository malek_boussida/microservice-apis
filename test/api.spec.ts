import axios, {AxiosResponse} from "axios";

describe("GET /healthcheck ", () => {
    test("/healthcheck should respond with code status 200 OK", async () => {
        const response : AxiosResponse = await axios.get('https://microservice-apis.vercel.app/api/healthcheck') //microapi.getHealthcheck();
        expect(response.status).toBe(200);
        expect(response.data).toEqual({
            statusCode:"200 OK"
        });
    });
});

describe("GET /dashboard ", () => {
    test("/dashboard should respond with code status 200 OK", async () => {
        const response = await axios.get('https://microservice-apis.vercel.app/api/dashboard')
        expect(response.status).toBe(200);
        expect(response.data).toEqual({
            statusCode: "200 OK",
            body: {
                iHaveData: true,
                myNameIs: 'Green-Got'
            }
        });
    });
});

describe("GET /* ", () => {
    test("It should respond with code status 404", async () => {
        let response ;
        try {
            response = await axios.get('https://microservice-apis.vercel.app/api/*');
        } catch (err) {
            response = err.response;
        }
        expect(response.status).toBe(404);

    });
});
